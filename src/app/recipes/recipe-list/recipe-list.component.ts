import { Recipe } from './../recipe.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('receta1', 'receta buena', 'https://i.pinimg.com/564x/54/88/9e/54889e1481bbef33a082bc63156acee6.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
